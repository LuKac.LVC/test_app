package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import org.springframework.data.repository.CrudRepository;

public interface CandidateRepository extends CrudRepository<Candidate, Integer> {}
