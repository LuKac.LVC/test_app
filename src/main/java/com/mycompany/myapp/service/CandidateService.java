package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.exception.BadRequestException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;

    /**
     * Import Candidate mannually
     */
    public Candidate importCandidateManually(Candidate candidate) {
        if (candidate.getFirstName() == null || candidate.getLastName() == null) {
            throw new BadRequestException("error.nullValue", null);
        }
        if (!candidate.getBirthYear().matches("[12][0-9]{3}")) {
            throw new BadRequestException("error.invalidYear", null);
        }
        candidateRepository.save(candidate);
        return candidate;
    }
    /**
     * Import Candidate from file excel
     */
    public List<Candidate> importCandidateByExcel(InputStream input) {
        List<Candidate> candidateList = new ArrayList<>();
        try {
            Workbook workbook = new XSSFWorkbook(input);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            rows.next();
            while (rows.hasNext()) {
                Row row = rows.next();
                Candidate candidate = new Candidate();
                if (row.getCell(0).getCellType() == CellType.STRING) {
                    candidate.setFirstName(row.getCell(0).getStringCellValue());
                }
                if (row.getCell(1).getCellType() == CellType.STRING) {
                    candidate.setLastName(row.getCell(1).getStringCellValue());
                }
                if (row.getCell(2).getCellType() == CellType.STRING) {
                    candidate.setBirthYear(row.getCell(2).getStringCellValue());
                }
                if (row.getCell(3).getCellType() == CellType.STRING) {
                    candidate.setPersonalNumber(row.getCell(3).getStringCellValue());
                }
                if (row.getCell(4).getCellType() == CellType.STRING) {
                    candidate.setGender(row.getCell(4).getStringCellValue());
                }
                if (row.getCell(5).getCellType() == CellType.STRING) {
                    candidate.setRegion(row.getCell(5).getStringCellValue());
                }
                if (row.getCell(6).getCellType() == CellType.STRING) {
                    candidate.setCountry(row.getCell(6).getStringCellValue());
                }
                if (row.getCell(7).getCellType() == CellType.STRING) {
                    candidate.setLinkedInLink(row.getCell(7).getStringCellValue());
                }
                if (row.getCell(8).getCellType() == CellType.STRING) {
                    candidate.setEmployer(row.getCell(8).getStringCellValue());
                }
                if (row.getCell(9).getCellType() == CellType.STRING) {
                    candidate.setFirstEmployment(row.getCell(9).getStringCellValue());
                }
                if (row.getCell(10).getCellType() == CellType.STRING) {
                    candidate.setManagerialPosition(row.getCell(10).getStringCellValue());
                }
                if (row.getCell(11).getCellType() == CellType.STRING) {
                    candidate.setBusinessAreas(row.getCell(11).getStringCellValue());
                }
                if (row.getCell(12).getCellType() == CellType.STRING) {
                    candidate.setcVS(row.getCell(12).getStringCellValue());
                }
                if (row.getCell(13).getCellType() == CellType.STRING) {
                    candidate.setContact(row.getCell(13).getStringCellValue());
                }
                this.importCandidateManually(candidate);
                candidateList.add(candidate);
            }
            workbook.close();
            input.close();
            return candidateList;
        } catch (IOException e) {
            throw new BadRequestException("error.uploadExcelWrongFormat", null);
        }
    }
/**
 * Get all Candidate
 */
    public List<Candidate> getAllCandidate() {
        return (List<Candidate>) candidateRepository.findAll();
    }
}
