package com.mycompany.myapp.service.mock;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
@ExtendWith(MockitoExtension.class)
public class CandidateServiceMockTest {
    @InjectMocks
    CandidateService candidateService;
    @Mock
    CandidateRepository candidateRepository;

    /**
     * Import candidate manually with firtName null
     */
    @Test
    public void testImportCandidateFirtNameNull(){
        //given
        Candidate candidate = new Candidate();
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * Import candidate manually with LastName null
     */
    @Test
    public void testImportCandidateLastNameNull(){
        //given
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setBirthYear("2001");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.nullValue");
    }
    /**
     * BirthYear wrong format
     */
    @Test
    public void testImportCandidateBirthYearFormat(){
        //given
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("4000");
        //When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateManually(candidate));
        //then
        assertThat(result.getMessage()).isEqualTo("error.invalidYear");
    }
    /**
     * Import Candidate Success
     */
    @Test
    public void testImortCandidateSuccess(){
        //given
        Candidate candidate = new Candidate();
        candidate.setFirstName("Cuong");
        candidate.setLastName("Tran");
        candidate.setBirthYear("2001");
        //When
        Candidate candidate1 = candidateService.importCandidateManually(candidate);
        //then
        assertThat(candidate1.getFirstName()).isEqualTo("Cuong");
        assertThat(candidate1.getLastName()).isEqualTo("Tran");
        assertThat(candidate1.getBirthYear()).isEqualTo("2001");
    }
}
